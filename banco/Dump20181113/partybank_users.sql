CREATE DATABASE  IF NOT EXISTS `partybank` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `partybank`;
-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: partybank
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '123456',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `telefone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bloqueado` tinyint(1) NOT NULL DEFAULT '0',
  `excluido` tinyint(1) DEFAULT '0',
  `cpf` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_token_UNIQUE` (`api_token`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Administrator','admin@test.com','$2y$10$C.Fd2GAq3muHE0VpZ38nTOfJcc6hMAQ2OtVA38vkKDZkfl0YyIFjS',NULL,'2018-02-08 17:19:01',NULL,'2018-03-12 18:58:04',NULL,0,0,'00000000011',NULL,0),(2,'Shany Oberbrunner','keebler.palma@gmail.com','$2y$10$C.Fd2GAq3muHE0VpZ38nTOfJcc6hMAQ2OtVA38vkKDZkfl0YyIFjS',NULL,'2018-02-08 17:19:01',NULL,'2018-03-12 18:58:10',NULL,0,1,'00000000012',NULL,0),(3,'Alessandra Orn','little.alexandra@yahoo.com','$2y$10$C.Fd2GAq3muHE0VpZ38nTOfJcc6hMAQ2OtVA38vkKDZkfl0YyIFjS',NULL,'2018-02-08 17:19:01',NULL,'2018-03-16 15:27:00',NULL,0,1,'00000000013',NULL,0),(4,'Adelle Murray','prudence80@hotmail.com','$2y$10$C.Fd2GAq3muHE0VpZ38nTOfJcc6hMAQ2OtVA38vkKDZkfl0YyIFjS',NULL,'2018-02-08 17:19:01',NULL,'2018-04-26 22:24:57',NULL,0,1,'00000000014',NULL,1),(5,'Florence Rau','jason73@schneider.net','$2y$10$C.Fd2GAq3muHE0VpZ38nTOfJcc6hMAQ2OtVA38vkKDZkfl0YyIFjS',NULL,'2018-02-08 17:19:01',NULL,'2018-03-16 15:27:14',NULL,0,1,'00000000015',NULL,0),(6,'Prof. Colt Pfannerstill','johnston.ernesto@hintz.biz','$2y$10$C.Fd2GAq3muHE0VpZ38nTOfJcc6hMAQ2OtVA38vkKDZkfl0YyIFjS',NULL,'2018-02-08 17:19:01',NULL,'2018-04-26 22:27:27',NULL,0,1,'00000000016',NULL,1),(7,'Prof. Lorenz Thiel III','bogan.avery@yahoo.com','$2y$10$C.Fd2GAq3muHE0VpZ38nTOfJcc6hMAQ2OtVA38vkKDZkfl0YyIFjS',NULL,'2018-02-08 17:19:01',NULL,'2018-03-12 16:05:18',NULL,0,1,'00000000017',NULL,0),(8,'Dr. Waldo Kub','carrie.dickinson@hotmail.com','$2y$10$C.Fd2GAq3muHE0VpZ38nTOfJcc6hMAQ2OtVA38vkKDZkfl0YyIFjS',NULL,'2018-02-08 17:19:01',NULL,'2018-03-12 19:45:42',NULL,0,1,'00000000018',NULL,0),(9,'Dr. Kattie Kreiger','kfarrell@yundt.info','$2y$10$C.Fd2GAq3muHE0VpZ38nTOfJcc6hMAQ2OtVA38vkKDZkfl0YyIFjS',NULL,'2018-02-08 17:19:01',NULL,'2018-03-12 19:45:26',NULL,0,1,'00000000019',NULL,0),(10,'Roxane Schinner','yazmin.barrows@hotmail.com','$2y$10$C.Fd2GAq3muHE0VpZ38nTOfJcc6hMAQ2OtVA38vkKDZkfl0YyIFjS',NULL,'2018-02-08 17:19:01',NULL,'2018-03-16 17:51:17',NULL,0,1,'00000000020',NULL,0),(14,'Bruno von','brunovon@gmail.com','123456',NULL,'2018-02-08 19:59:12',NULL,'2018-05-14 01:09:56',NULL,1,1,'00000000021',NULL,1),(61,'André von Glehn','glehnandre@gmail.com','$2y$10$QTDTMivMBWsSvQbVTluzu.nxYag8y1cjNZDajEVfPJIvGh.iJmF5y','cgQgqyMduu9uL7mnU8bc0nCHHCSDeimB3U8PUfrme6iAyPTORgyt6XEClfeN','2018-03-16 17:46:53',NULL,'2018-07-27 20:27:18','61992711100',0,0,'90511549172','wjOBpJwhfPh7gTLTyFxR2NJ64YgYZIdeW9BGNofVz7L04gJxbgwxmeXr0RXl',1),(62,'Usuário 1','andre.glehn@stf.jus.br','$2y$10$9pWQGRkGIZsoZNyiIKiHD.80yNgOM875Q5iic1Sgv1PG5CLuLfxDG','dKnZJux1IVI5NUxxKHjKA9dNZcbN1ikXOuCVEqCLucGc86J9bwQq9JhyQgJO','2018-03-20 15:38:56',NULL,'2018-03-20 15:38:56',NULL,0,0,'00000000001','lnlF9VA1bhLTCOT9CZ1KvtYkBj00B7oOfwacdxid1EM59W0G8FHgqpLiNx4S',0),(63,'Testando 1','andre.pereira@stf.jus.br','$2y$10$OhOuWbfIOtluw2Sg.CVC8./hSejKiJk5gbpSi8iS5wNlrpLmlhYje','dfuG00ZXrXv1f2W60YOSDumNpUEzrjD1jFr5j75ciZp5H9iI9vSBrO4smt8y','2018-04-19 22:38:23',NULL,'2018-04-19 22:38:23',NULL,0,0,'00000000003','3jVb8t9M99QMmuS00KdiBcWZMZSFEcKW1oikftuCoBRK0RZKM4bf44YrvMEe',0),(79,'Marcela Prado','marcela_meg@hotmail.com','$2y$10$BSTNa0XYMSd.VzqY2yZ/D.porjSMjPliSzEu6LqM0/Dq.GHCwB8xu','sTWeL1Z6Ud3AYmUAkVbhL3etFQhnw1xMYYEjaXLICVqEHG2z5k3PMshRXRxq','2018-04-20 19:01:15',NULL,'2018-04-20 19:01:15',NULL,0,0,'00000000009','gODVH9LFiM1LkiVkXumHe1fJSXgOLSLtjsDPY7aX27FLrOr6R8n6z4E8CmhZ',0),(80,'Leandro Martins','leandrobm@gmail.com','$2y$10$EEjteTF3KRRBnOYbFmWdQ.6xazOrnjcf/nmN3T4B.tLs2dljpPXPy','9IwjHAmiopLKR2WZNiaDfBuo3JgkUBnfc8aYyYQQXDahrXX0OA7lKiOlXogQ','2018-04-21 16:22:24',NULL,'2018-04-21 16:22:24',NULL,0,0,'00000000010','aw72bsfiLY6gGmkQp3eowquI0qUg0pGtE1ecL2t8d3w9L3X5Z2KEM7eVc1ff',0),(83,'Vitor Soares','vitorsoares@gmail.com','123456',NULL,'2018-04-27 17:56:50',NULL,'2018-04-27 17:56:50','61992711100',0,0,'00000000007',NULL,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-13 23:23:23
