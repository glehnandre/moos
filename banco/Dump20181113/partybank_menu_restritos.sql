CREATE DATABASE  IF NOT EXISTS `partybank` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `partybank`;
-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: partybank
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `menu_restritos`
--

DROP TABLE IF EXISTS `menu_restritos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_restritos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `evento_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `cartao_id` int(10) unsigned DEFAULT NULL,
  `quantidade` int(11) DEFAULT '0' COMMENT 'Indica a quantidade máxima que pode ser consumida no mês',
  `restrito` tinyint(1) DEFAULT '0' COMMENT 'Indica se o item de menu é restrito, não pode ser consumido.',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `frequencia` int(1) DEFAULT NULL COMMENT '1 - Dia\n2 - Semana\n3 - Mês',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_restritos`
--

LOCK TABLES `menu_restritos` WRITE;
/*!40000 ALTER TABLE `menu_restritos` DISABLE KEYS */;
INSERT INTO `menu_restritos` VALUES (39,24,41,61,24,0,1,'2018-07-16 18:21:41','2018-07-16 20:10:24',NULL),(40,31,41,61,24,1,0,'2018-07-16 18:21:41','2018-07-16 20:10:24',NULL),(41,32,41,61,24,0,0,'2018-07-16 18:21:41','2018-07-16 20:10:24',NULL),(42,33,41,61,24,5,0,'2018-07-16 18:21:41','2018-07-16 20:10:24',NULL),(43,37,41,61,24,1,0,'2018-07-16 18:21:41','2018-07-16 20:10:24',2),(44,38,41,61,24,0,0,'2018-07-16 18:21:41','2018-07-16 20:10:24',NULL),(45,39,41,61,24,0,0,'2018-07-16 18:21:41','2018-07-16 20:10:24',NULL);
/*!40000 ALTER TABLE `menu_restritos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-13 23:23:38
