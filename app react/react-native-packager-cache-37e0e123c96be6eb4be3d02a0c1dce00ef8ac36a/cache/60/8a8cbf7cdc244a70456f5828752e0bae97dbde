/**
 * @file Converts argument to a value that can be used as a property key.
 * @version 2.0.2
 * @author Xotic750 <Xotic750@gmail.com>
 * @copyright  Xotic750
 * @license {@link <https://opensource.org/licenses/MIT> MIT}
 * @module to-property-key-x
 */

'use strict';

var hasSymbols = require('has-symbol-support-x');
var toPrimitive = require('to-primitive-x');
var toStr = require('to-string-x');

module.exports = function toPropertyKey(argument) {
  var key = toPrimitive(argument, String);
  return hasSymbols && typeof key === 'symbol' ? key : toStr(key);
};