'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (db, dataDocs, _ref, callback) {
  var binaryAttachments = _ref.binaryAttachments;

  var attachmentKeys = dataDocs.reduce(function (res, data) {
    data && data._attachments && Object.keys(data._attachments).forEach(function (key) {
      res.push((0, _keys.forAttachment)(data._attachments[key].digest));
    });
    return res;
  }, []);

  db.storage.multiGet(attachmentKeys, function (error, attachments) {
    if (error) return callback(error);

    var attachmentObj = attachments.reduce(function (res, attachment) {
      if (attachment) res[attachment.digest] = attachment;
      return res;
    }, {});

    dataDocs.forEach(function (doc) {
      doc && doc._attachments && Object.keys(doc._attachments).forEach(function (key) {
        var newAttachment = babelHelpers.extends({}, attachmentObj[doc._attachments[key].digest]);
        if (newAttachment) {
          doc._attachments[key] = newAttachment;
          if (binaryAttachments) {
            var contentType = doc._attachments[key].content_type;
            doc._attachments[key].data = (0, _pouchdbBinaryUtils.base64StringToBlobOrBuffer)(doc._attachments[key].data, contentType);
          }
        }
      });
    });

    callback();
  });
};

var _pouchdbBinaryUtils = require('pouchdb-binary-utils');

var _keys = require('./keys');