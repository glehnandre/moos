'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (db, opts, callback) {
  var filterKey = 'key' in opts ? opts.key : false;
  var skip = opts.skip || 0;
  var limit = typeof opts.limit === 'number' ? opts.limit : -1;
  var includeDeleted = 'deleted' in opts ? opts.deleted === 'ok' : false;
  var includeDoc = 'include_docs' in opts ? opts.include_docs : true;
  var includeAttachments = 'attachments' in opts ? opts.attachments : false;
  var binaryAttachments = 'binary' in opts ? opts.binary : false;
  var includeConflicts = 'conflicts' in opts ? opts.conflicts : false;
  var descending = 'descending' in opts && opts.descending;
  var startkey = descending ? 'endkey' in opts ? opts.endkey : false : 'startkey' in opts ? opts.startkey : false;
  var endkey = descending ? 'startkey' in opts ? opts.startkey : false : 'endkey' in opts ? opts.endkey : false;
  var excludeStart = descending && !(opts.inclusive_end !== false);
  var inclusiveEnd = descending || opts.inclusive_end !== false;

  var docToRow = function docToRow(doc) {
    var result = {
      id: doc.id,
      key: doc.id,
      value: {
        deleted: doc.deleted,
        rev: doc.winningRev
      }
    };

    if (includeDoc && !doc.deleted) {
      result.doc = babelHelpers.extends({}, doc.data, {
        _id: doc.id,
        _rev: doc.winningRev
      });

      if (includeConflicts) {
        result.doc._conflicts = (0, _pouchdbMerge.collectConflicts)(doc);
      }
    }

    return result;
  };

  getDocs(db, { filterKey: filterKey, startkey: startkey, endkey: endkey, skip: skip, limit: limit, excludeStart: excludeStart, inclusiveEnd: inclusiveEnd, includeAttachments: includeAttachments, binaryAttachments: binaryAttachments, includeDeleted: includeDeleted, descending: descending }, function (error, docs) {
    if (error) return callback((0, _pouchdbErrors.generateErrorFromResponse)(error));

    var rows = docs.map(docToRow);

    callback(null, {
      total_rows: db.meta.doc_count,
      offset: skip,
      rows: rows
    });
  });
};

var _pouchdbErrors = require('pouchdb-errors');

var _pouchdbMerge = require('pouchdb-merge');

var _keys = require('./keys');

var _inline_attachments = require('./inline_attachments');

var _inline_attachments2 = babelHelpers.interopRequireDefault(_inline_attachments);

var getDocs = function getDocs(db, _ref, callback) {
  var filterKey = _ref.filterKey,
      startkey = _ref.startkey,
      endkey = _ref.endkey,
      skip = _ref.skip,
      limit = _ref.limit,
      excludeStart = _ref.excludeStart,
      inclusiveEnd = _ref.inclusiveEnd,
      includeDeleted = _ref.includeDeleted,
      includeAttachments = _ref.includeAttachments,
      binaryAttachments = _ref.binaryAttachments,
      descending = _ref.descending;

  db.storage.getKeys(function (error, keys) {
    if (error) return callback(error);

    var filterKeys = (0, _keys.getDocumentKeys)(keys).filter(function (key) {
      if (startkey && startkey > key) return false;
      if (excludeStart && startkey && startkey === key) return false;
      if (endkey) return inclusiveEnd ? endkey >= key : endkey > key;
      if (filterKey) return filterKey === key;

      return true;
    });

    db.storage.multiGet((0, _keys.toDocumentKeys)(filterKeys), function (error, docs) {
      if (error) return callback(error);

      var result = includeDeleted ? docs : docs.filter(function (doc) {
        return !doc.deleted;
      });

      if (descending) result = result.reverse();
      if (skip > 0) result = result.slice(skip);
      if (limit >= 0 && result.length > limit) result = result.slice(0, limit);

      var seqKeys = result.map(function (item) {
        return (0, _keys.forSequence)(item.rev_map[item.winningRev]);
      });
      db.storage.multiGet(seqKeys, function (error, dataDocs) {
        if (error) return callback(error);

        var dataObj = dataDocs.reduce(function (res, data) {
          if (data) res[data._id] = data;
          return res;
        }, {});

        if (!includeAttachments) {
          return callback(null, result.map(function (item) {
            item.data = dataObj[item.id];
            return item;
          }));
        }

        (0, _inline_attachments2.default)(db, dataDocs, { binaryAttachments: binaryAttachments }, function (error) {
          if (error) return callback(error);

          return callback(null, result.map(function (item) {
            item.data = dataObj[item.id];
            return item;
          }));
        });
      });
    });
  });
};